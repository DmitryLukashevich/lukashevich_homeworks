package ru.lukas;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * @author Dmitry Lukashevich
 */
public class Homework21 {

    private static final Random random = new Random();
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter the size of an array");
        int[] array = initArray(scanner.nextInt());

        // Multi-threaded calculation
        System.out.println("Enter the number of threads");
        System.out.println("The sum of elements is: "
                + new ConcurrentIntArraySum(array).getSum(scanner.nextInt()));

        // Single-threaded calculation to compare with multi-threaded one
        System.out.print("Single-threaded calculation: ");
        System.out.println(Arrays.stream(array).reduce(Integer::sum).orElse(0));
    }

    private static int[] initArray(int size) {
        int[] array = new int[size];

        for (int i = 0; i < size; i++) {
            array[i] = random.nextInt(100);
        }

        return array;
    }
}
