package ru.lukas;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Dmitry Lukashevich
 */
public class ConcurrentIntArraySum {

    private final int[] array;
    private final AtomicInteger sum;

    public ConcurrentIntArraySum(int[] array) {
        this.array = array;
        sum = new AtomicInteger();
    }

    public int getSum(int numberOfThreads) {
        Thread[] threads = new Thread[numberOfThreads];
        int step = array.length / numberOfThreads;

        for (int i = 0; i < numberOfThreads; i++) {
            int lowerBound = i * step;
            int upperBound = i == numberOfThreads - 1 ? array.length : (i + 1) * step;

            threads[i] = new SumThread("Thread " + i, lowerBound, upperBound);
            threads[i].start();
        }

        joinAll(threads); // waiting for threads to finish calculation

        return sum.get();
    }

    private static void joinAll(Thread[] threads) {
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e.getMessage());
            }
        }
    }

    private class SumThread extends Thread {

        private final int from, to;

        private SumThread(String name, int from , int to) {
            super(name);
            this.from = from;
            this.to = to;
        }

        @Override
        public void run() {
            System.out.println(this.getName() + " started. Index range: "
                    + "from " + from + " to " + to);

            int localSum = 0;

            for (int i = from; i < to; i++) {
                localSum += array[i];
            }

            sum.getAndAdd(localSum);
            System.out.println(this.getName() + " has finished");
        }
    }
}
