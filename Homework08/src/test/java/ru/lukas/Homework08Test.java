package ru.lukas;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class Homework08Test {

    @Test
    public void shouldSortByWeightInNaturalOrder() {
        // Mock data
        Person[] persons = {
                new Person("Andrey", 78.2f),
                new Person("Masha", 54.8f),
                new Person("Semyon", 92.6f),
                new Person("Dmitry", 66.2f),
                new Person("Marta", 68.4f)
        };
        // Mock sorted data
        Person[] sortedByWeightPersons = { persons[1], persons[3], persons[4], persons[0], persons[2] };
        // Sort mocked data
        Homework08.sort(persons);
        // Compare sorted data with mocked-sorted one
        assertThat(persons, equalTo(sortedByWeightPersons));
    }
}
