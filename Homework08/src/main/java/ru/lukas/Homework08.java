package ru.lukas;

import java.util.Scanner;

/**
 * @author Dmitry Lukashevich
 */
public class Homework08 {

    public static void main(String[] args) {
        /*
         * Pass a number as an argument to set the number of Persons in an array.
         * Default is 10.
         */
        Person[] persons = new Person[args.length > 0 ? Integer.parseInt(args[0]) : 10];

        initializePersonsArray(persons);
        sort(persons);
        /* We can also sort with Arrays.sort since our Person class implements Comparable interface
         *     Arrays.sort(persons);
         */
        System.out.println("================== Persons sorted by weight ===================");
        for (Person person : persons) {
            System.out.println("Name: " + person.getName() + "   Weight: " + person.getWeight() + "kg");
        }
        System.out.println("========================= End of list =========================");
    }

    private static void initializePersonsArray(Person[] persons) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the name and weight of the person separated by space. For ex. \"John 54.7\":");
        for (int i = 0; i < persons.length; i++) {
            String[] data = scanner.nextLine().split("\\s");

            // Initializing using setters
            persons[i] = new Person();
            persons[i].setName(data[0]);
            persons[i].setWeight(Float.parseFloat(data[1].replace(",", ".")));

            /* or using constructor
             *     persons[i] = new Person(data[0], Float.parseFloat(data[1].replace(",", ".")));
             */
        }
        System.out.println();
    }

    /*
     * Since the number of elements is pretty low, I use BubbleSort here for the sake of simplicity.
     */
    public static void sort(Person[] persons) {
        for (int i = persons.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (persons[j].getWeight() > persons[j + 1].getWeight()) {
                    Person tmp = persons[j];
                    persons[j] = persons[j + 1];
                    persons[j + 1] = tmp;
                }
            }
        }
    }

    /* ================= Insertion sort ==================================
    public static void sort(Person[] persons) {
        for (int i = 1; i < persons.length; i++) {
            int j = i - 1;
            Person tmp = persons[i];

            while (j >= 0 && persons[j].getWeight() > tmp.getWeight()) {
                persons[j + 1] = persons[j--];
            }
            persons[j + 1] = tmp;
        }
    }
    =================================================================== */
}
