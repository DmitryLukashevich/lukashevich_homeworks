package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class Person implements Comparable<Person> {

    private String name;
    private float weight;

    // This constructor is used in application
    public Person() {}

    // This constructor is used for testing purposes
    public Person(String name, float weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    /*
     * Implementing Comparable interface to sort instances of this class by weight.
     *
     * Not required in the actual homework
     */
    @Override
    public int compareTo(Person person) {
        return Float.compare(this.weight, person.weight);
    }
}
