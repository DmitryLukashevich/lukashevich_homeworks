package ru.lukas;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.util.Comparator;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * @author Dmitry Lukashevich
 */
public class Homework20 {

    private static final String filename = "car.db";
    private static final CarMapper carMapper = new CarMapper();

    public static void main(String[] args) {
        System.out.println("=====> Plate numbers of all black cars, or cars with 0 mileage <=====");
        execute(Homework20::printPlateNumbersOfBlackOrNewCars);
        System.out.println("=====================================================================");

        System.out.print("=====> The number of unique models with price between 700000 and 800000 is: ");
        execute(stream ->
                printNumberOfUniqueCarModelsWithPriceBetween(stream, 700000, 800000));

        System.out.print("=====> The color of the cheapest car is: ");
        execute(Homework20::printColorOfCheapestCar);

        System.out.print("=====> The average price for Camry models is: ");
        execute(stream -> printAveragePriceForCarModel(stream, "Camry"));
    }

    /**
     * Executor method that takes Consumer<Stream<Car>> as a parameter,
     * creates BufferedReader for text file, gets text lines split by "|"
     * maps that data into Car object and invokes consumer's accept() method
     * with that Stream as an input.
     *
     * @param consumer method that operates on Stream<Car>
     */
    private static void execute(Consumer<Stream<Car>> consumer) {
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            consumer.accept(br.lines()
                    .map(line -> line.split("\\|"))
                    .map(carMapper::toCarObject));
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private static void printPlateNumbersOfBlackOrNewCars(Stream<Car> stream) {
        stream.filter(car -> car.getColor().equals("Black") || car.getMileage() == 0)
                .forEach(car -> System.out.println(car.getPlateNumber()));
    }

    private static void printNumberOfUniqueCarModelsWithPriceBetween(
            Stream<Car> stream, int min, int max)
    {
        System.out.println(stream
                .filter(car -> car.getPrice() >= min && car.getPrice() <= max)
                .map(Car::getModel)
                .distinct().count());
    }

    private static void printColorOfCheapestCar(Stream<Car> stream) {
        System.out.println(stream
                .min(Comparator.comparing(Car::getPrice))
                .map(Car::getColor)
                .orElseThrow(() -> new RuntimeException("No cars have been found, the file is empty or corrupted.")));
    }

    private static void printAveragePriceForCarModel(
            Stream<Car> stream, String model)
    {
        System.out.println(stream
                .filter(car -> car.getModel().equals(model))
                .mapToInt(Car::getPrice)
                .average()
                .orElseThrow(() -> new RuntimeException(model + " model doesn't exist.")));
    }
}
