package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class CarMapper {

    public Car toCarObject(String[] data) {
        Car car = new Car();

        car.setPlateNumber(data[0]);
        car.setModel(data[1]);
        car.setColor(data[2]);
        car.setMileage(Integer.parseInt(data[3]));
        car.setPrice(Integer.parseInt(data[4]));

        return car;
    }

    /*
     * Here we can also implement   public String[] toRawData(Car car);   method
     * to map Car objects back into raw data if needed
     */
}
