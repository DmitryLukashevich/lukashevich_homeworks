package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class LinkedList<E> {

    private Node<E> first;
    private Node<E> last;

    private int size = 0;

    public void addFirst(E value) {
        if ((size++) == 0) {
            first = new Node<>(value, null, null);
            last = first;
        } else {
            first.prev = new Node<>(value, first, null);
            first = first.prev;
        }
    }

    public void addLast(E value) {
        if ((size++) == 0) {
            last = new Node<>(value, null, null);
            first = last;
        } else {
            last.next = new Node<>(value, null, last);
            last = last.next;
        }
    }

    public E get(int index) {
        return getNode(index).value;
    }

    public E removeAt(int index){
        Node<E> node = getNode(index);

        node.prev.next = node.next;
        node.next.prev = node.prev;
        size--;

        return node.value;
    }

    public E removeFirst() {
        Node<E> node = first;

        first = first.next;
        first.prev = null;
        size--;

        return node.value;
    }

    public E removeLast() {
        Node<E> node = last;

        last = last.prev;
        last.next = null;
        size--;

        return node.value;
    }

    public void clear() {
        first = last = null;
        size = 0;
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("[ ");
        Node<E> current = first;

        for (int i = 0; i < size; i++) {
            stringBuilder.append(current.value).append(" ");
            current = current.next;
        }

        return stringBuilder.append("]").toString();
    }

    /*
     * Util method that traverses the list 'till it get to the required index.
     * It goes from head to tail if index is smaller than size / 2
     * and backwards from tail to head if index is greater or equal to size / 2
     */
    private Node<E> getNode(int index) {
        checkBounds(index);
        Node<E> current;

        if (index < size / 2) {
            current = first;
            // oneliner --> for (int i = 0; i != index; current = current.next, i++);
            for (int i = 0; i != index; i++) {
                current = current.next;
            }
        } else {
            current = last;
            // for (int i = size - 1; i != index; current = current.prev, i--);
            for (int i = size - 1; i != index; i--) {
                current = current.prev;
            }
        }

        return current;
    }

    private void checkBounds(int index) {
        if (index < 0 || index >= size) { throw new IndexOutOfBoundsException(index); }
    }

    private static class Node<E> {

        private Node<E> next;
        private Node<E> prev;

        private final E value;

        Node(E value, Node<E> next, Node<E> prev) {
            this.value = value;
            this.next = next;
            this.prev = prev;
        }
    }
}