package ru.lukas;

import java.util.Random;

/**
 * @author Dmitry Lukashevich
 */
public class Homework16 {

    private static final Random random = new Random();

    public static void main(String[] args) {
        System.out.println("====================> ArrayList tests <=====================");
        testArrayList();
        System.out.println("=================> end of ArrayList tests <=================\n");

        System.out.println("====================> LinkedList tests <=====================");
        testLinkedList();
        System.out.println("=================> end of LinkedList tests <=================\n");
    }

    private static void testArrayList() {
        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            list.add(random.nextInt());
        }

        // Testing add method
        System.out.println("=====> An initial array: " + list);

        // Testing get method
        System.out.println("=====> Seventh element of array is: " + list.get(7));

        // Testing removeAt method
        Integer element1 = list.removeAt(2);
        Integer element2 = list.removeAt(15);
        System.out.println("=====> Array after removal of elements 2 and 15: " + list);
        System.out.println("=====> Removed elements are: " + element1 + " and " + element2);

        // Testing clear method
        list.clear();
        System.out.println("=====> Array after clearing: " + list);
    }

    private static void testLinkedList() {
        LinkedList<Integer> list = new LinkedList<>();

        for (int i = 0; i < 20; i++) {
            list.addFirst(random.nextInt());
        }

        // Testing addFirst method
        System.out.println("=====> Linked list: " + list);

        // Testing get method
        System.out.println("=====> Seventh element of linked list is: " + list.get(7));

        // Testing addLast method
        list.addLast(986534);
        System.out.println("=====> Using addLast method with number 986534: " + list);

        // Testing removeAt, removeFirst and removeLast methods
        Integer element1 = list.removeAt(4);
        Integer element2 = list.removeAt(16);
        System.out.println("=====> List after removal of elements 4 and 16: " + list);
        System.out.println("=====> Removed elements are: " + element1 + " and " + element2);
        System.out.println("=====> First element removed is: " + list.removeFirst());
        System.out.println("=====> Last element removed is: " + list.removeLast());
        System.out.println("=====> List after removing the first and last elements: " + list);

        // Testing clear method
        list.clear();
        System.out.println("=====> Linked list after clearing: " + list);
    }
}
