package ru.lukas;

import java.util.Arrays;

/**
 * @author Dmitry Lukashevich
 */
public class ArrayList<E>  {

    private static final int DEFAULT_CAPACITY = 10;

    private int size;
    private E[] elements;

    public ArrayList() {
        elements = (E[]) new Object[DEFAULT_CAPACITY];
        size = 0;
    }

    public void add(E element) {
        if (size == elements.length) { resize(); }

        elements[size++] = element;
    }

    public E get(int index) {
        checkBounds(index);

        return elements[index];
    }

    public E removeAt(int index) {
        checkBounds(index);
        E element = elements[index];

        System.arraycopy(elements, index + 1, elements, index, (size--) - index);

        return element;
    }

    public void clear() {
        for (int i = 0; i < size; i++) {
            elements[i] = null;
        }

        size = 0;
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("[ ");

        for (int i = 0; i < size; i++) {
            stringBuilder.append(elements[i]).append(" ");
        }

        return stringBuilder.append("]").toString();
    }

    private void resize() {
        E[] oldElements = elements;

        elements = Arrays.copyOf(oldElements, oldElements.length + oldElements.length / 2);
    }

    private void checkBounds(int index) {
        if (index >= size || index < 0) { throw new IndexOutOfBoundsException(index); }
    }
}
