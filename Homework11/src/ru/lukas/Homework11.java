package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class Homework11 {

    public static void main(String[] args) {
        Logger logger = Logger.getInstance();

        logger.log("Example message from logger");
        logger.log("And one more time :)");
    }
}
