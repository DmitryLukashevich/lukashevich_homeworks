package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class Logger {
    /*
     * We actually don't need static initializer here
     *     private static final Logger instance = new Logger();
     * would get the same result with less code :)
     */
    private static final Logger instance;

    static {
        instance = new Logger();
    }

    private Logger() {}

    public static Logger getInstance() {
        return instance;
        /*
         * Another way to do this is lazy initialization.
         * Instance would be created only when getInstance() method is called.
         * That's a bad idea in multithreading environment due to possible synchronization issues
         *     if (instance == null) {
         *         instance = new Logger();
         *     }
         *
         *     return instance;
         */
    }

    public void log(String message) {
        System.out.println(message);
    }
}
