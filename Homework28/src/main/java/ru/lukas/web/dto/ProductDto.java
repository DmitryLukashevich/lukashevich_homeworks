package ru.lukas.web.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Dmitry Lukashevich
 */
@Data
@Builder
public class ProductDto {

    private String description;
    private BigDecimal price;
    private Integer amount;
}
