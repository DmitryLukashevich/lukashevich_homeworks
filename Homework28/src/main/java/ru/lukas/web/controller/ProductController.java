package ru.lukas.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import ru.lukas.web.component.ProductMapper;
import ru.lukas.web.dto.ProductDto;
import ru.lukas.web.service.ProductService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Dmitry Lukashevich
 */
@Controller
public class ProductController {

    private final ProductService productService;
    private final ProductMapper productMapper;

    public ProductController(ProductService productService, ProductMapper productMapper) {
        this.productService = productService;
        this.productMapper = productMapper;
    }

    @GetMapping("/")
    public String getAllProducts(Model model) {
        List<ProductDto> products = productService.getAllProducts().stream()
                .map(productMapper::toDto).collect(Collectors.toList());

        model.addAttribute("products", products);

        return "index";
    }

    @GetMapping("/product")
    public String addProduct() { return "product_add"; }

    @PostMapping(value = "/product/add", consumes = "application/x-www-form-urlencoded")
    public String addProduct(ProductDto product) {
        productService.addProduct(productMapper.toModel(product));

        return "redirect:/product";
    }
}
