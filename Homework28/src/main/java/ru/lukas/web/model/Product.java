package ru.lukas.web.model;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Dmitry Lukashevich
 */
@Data
@Builder
public class Product {

    private Integer id;
    private String description;
    private BigDecimal price;
    private Integer amount;
}
