package ru.lukas.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Homework28 {

    public static void main(String[] args) {
        SpringApplication.run(Homework28.class, args);
    }

}
