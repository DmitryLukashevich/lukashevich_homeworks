package ru.lukas.web.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import org.springframework.stereotype.Repository;
import ru.lukas.web.model.Product;

import javax.sql.DataSource;
import java.util.List;

/**
 * @author Dmitry Lukashevich
 */
@Repository
public class ProductRepositoryImpl implements ProductRepository {

    private final JdbcTemplate jdbcTemplate;

    //language=SQL
    private static final String SQL_FIND_ALL =
            "SELECT id, description, price, amount FROM product";

    //language=SQL
    private static final String SQL_SAVE = "INSERT INTO product " +
            "(description, price, amount) VALUES (?, ?::MONEY, ?)";

    private static final RowMapper<Product> productRowMapper = (row, rn) -> Product.builder()
            .id(row.getInt("id"))
            .description(row.getString("description"))
            .price(row.getBigDecimal("price"))
            .amount(row.getInt("amount"))
            .build();

    public ProductRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_FIND_ALL, productRowMapper);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_SAVE,
                product.getDescription(),
                product.getPrice(),
                product.getAmount());
    }
}
