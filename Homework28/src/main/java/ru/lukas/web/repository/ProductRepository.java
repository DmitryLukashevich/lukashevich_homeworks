package ru.lukas.web.repository;

import ru.lukas.web.model.Product;

import java.util.List;

/**
 * @author Dmitry Lukashevich
 */
public interface ProductRepository {

    List<Product> findAll();

    void save(Product product);
}
