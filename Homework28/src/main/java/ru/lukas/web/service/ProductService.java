package ru.lukas.web.service;

import org.springframework.stereotype.Service;
import ru.lukas.web.model.Product;
import ru.lukas.web.repository.ProductRepository;

import java.util.List;

/**
 * @author Dmitry Lukashevich
 */
@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public void addProduct(Product product) {
        /*
         * Here may be a complicated logic,
         * that's why we need this service layer so that controller is nice and clean
         */
        productRepository.save(product);
    }
}
