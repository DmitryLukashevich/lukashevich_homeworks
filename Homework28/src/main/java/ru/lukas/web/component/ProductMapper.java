package ru.lukas.web.component;

import org.springframework.stereotype.Component;

import ru.lukas.web.dto.ProductDto;
import ru.lukas.web.model.Product;

/**
 * @author Dmitry Lukashevich
 */
@Component
public class ProductMapper {

    public Product toModel(ProductDto productDto) {
        return Product.builder()
                .description(productDto.getDescription())
                .price(productDto.getPrice())
                .amount(productDto.getAmount())
                .build();
    }

    public ProductDto toDto(Product product) {
        return ProductDto.builder()
                .description(product.getDescription())
                .price(product.getPrice())
                .amount(product.getAmount())
                .build();
    }
}
