DROP TABLE IF EXISTS "order", product, customer;

CREATE TABLE customer (
    id SERIAL PRIMARY KEY,
    name VARCHAR(30) NOT NULL,
    lastname VARCHAR(50) NOT NULL
);

CREATE TABLE product (
    id SERIAL PRIMARY KEY,
    description VARCHAR(200) NOT NULL,
    price MONEY NOT NULL,
    amount INT CHECK ( amount >= 0 ) NOT NULL
);

CREATE TABLE "order" (
    product_id INT NOT NULL,
    customer_id INT NOT NULL,
    order_date DATE NOT NULL,
    product_amount INT CHECK ( product_amount >= 0 ) NOT NULL,
    FOREIGN KEY (product_id) REFERENCES product(id) ON DELETE CASCADE,
    FOREIGN KEY (customer_id) REFERENCES customer(id) ON DELETE CASCADE
);

INSERT INTO customer
    (name, lastname)
VALUES
    ('Maxim', 'Kotov'),
    ('Egor', 'Salamatov'),
    ('Diana', 'Krotova'),
    ('Pavel', 'Savin'),
    ('Dmitry', 'Kolosov'),
    ('Angelina', 'Buldina'),
    ('Veronika', 'Medvedeva'),
    ('Anna', 'Sedakova'),
    ('George', 'Smith'),
    ('Rudolf', 'Kopf');

INSERT INTO product
    (description, price, amount)
VALUES
    ('Big Coke', 12.00, 2050),
    ('Delicious fried chicken', 24.99, 1090),
    ('Nuggets with cheese, oh my gosh soooo taasty', 15.99, 12000),
    ('Crispy lispy fries', 9.99, 9765),
    ('Big Beef Meatballs with Bucatini, hell yeaaah', 46.00, 1264),
    ('Pepperoni pizza with jalapenos', 34.99, 6754);

INSERT INTO "order"
    (product_id, customer_id, order_date, product_amount)
VALUES
    (2, 1, '2021-10-15', 2),
    (3, 4, '2021-10-21', 1),
    (6, 7, '2021-11-01', 5),
    (4, 2, '2021-11-26', 3),
    (1, 8, '2021-09-11', 15),
    (3, 1, '2021-09-11', 5);
