package ru.lukas;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class Homework06Test {

    @Test
    public void shouldReturnCorrectIndexOfTheNumberInArray() {
        int[] array = { 12, 124, 9, 18, 0, 6 };

        assertThat(Homework06.findIndexInArray(array, 9), equalTo(2));
        assertThat(Homework06.findIndexInArray(array, 0), equalTo(4));
        assertThat(Homework06.findIndexInArray(array, 6), equalTo(5));
        assertThat(Homework06.findIndexInArray(array, 48), equalTo(-1));
    }

    @Test
    public void shouldShiftAllZerosToTheEndOfArray() {
        int[] array1 = { 12, 124, 0, 18, 0, 6 };
        int[] array2 = { 0, 124, 0, 0, 0, 6, 18, 152 };
        int[] array3 = { 0, 0, 0, 0, 0, 6, 2, 14, 0, 79, 12, 5645, 23, 0, 0, 533, 7, 0 };
        int[] array4 = { 60, 124, 5, 1, 45, 6, 18, 152 };

        Homework06.shiftNonZerosLeft(array1);
        Homework06.shiftNonZerosLeft(array2);
        Homework06.shiftNonZerosLeft(array3);
        Homework06.shiftNonZerosLeft(array4);

        assertThat(array1, equalTo(new int[] { 12, 124, 18, 6, 0, 0 }));
        assertThat(array2, equalTo(new int[] { 124, 6, 18, 152, 0, 0, 0, 0 }));
        assertThat(array3, equalTo(new int[] { 6, 2, 14, 79, 12, 5645, 23, 533, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0 }));
        assertThat(array4, equalTo(new int[] { 60, 124, 5, 1, 45, 6, 18, 152 }));
    }
}
