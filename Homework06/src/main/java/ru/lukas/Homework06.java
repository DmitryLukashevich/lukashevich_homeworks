package ru.lukas;

import java.util.Arrays;

public class Homework06 {

    public static void main(String[] args) {
        int[] array = { 132, 0, 54, 0, 0, 8, 12, 79, 100, 0 };

        System.out.println("In array " + Arrays.toString(array) + " number 12 has index " + findIndexInArray(array, 12));
        System.out.print("Array " + Arrays.toString(array) + " after shifting all non-zero elements to the left ");
        shiftNonZerosLeft(array);
        System.out.println("looks like " + Arrays.toString(array));
    }

    public static int findIndexInArray(int[] array, int searchedElement) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == searchedElement) { return i; }
        }

        return -1;
    }

    public static void shiftNonZerosLeft(int[] array) {
        int nonZeroElementPosition = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0) { array[nonZeroElementPosition++] = array[i]; }
        }

        for (; nonZeroElementPosition < array.length; nonZeroElementPosition++) {
            array[nonZeroElementPosition] = 0;
        }
    }
}
