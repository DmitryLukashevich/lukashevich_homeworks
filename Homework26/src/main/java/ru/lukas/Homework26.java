package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class Homework26 {

    public static void main(String[] args) {
        NumbersUtil numbersUtil = new NumbersUtil();

        System.out.println(numbersUtil.gcd(16, 89036));
        System.out.println(numbersUtil.gcd(12, 145));
    }
}
