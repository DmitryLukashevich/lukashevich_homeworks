package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class NumbersUtil {

    public int gcd(int a, int b) {
        if (a <= 0 || b <= 0) {
            throw new IllegalArgumentException("Arguments must be non-zero, positive numbers");
        }

        int remainder = a % b;

        if (remainder == 0) { return b; }
        return gcd(b, remainder);
    }
}
