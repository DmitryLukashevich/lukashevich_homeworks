package ru.lukas;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Dmitry Lukashevich
 */
@DisplayName(value = "NumbersUtil tests")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
public class NumbersUtilTest {

    private final NumbersUtil numberUtils = new NumbersUtil();

    @Nested
    @DisplayName(value = "gcd() method tests")
    public class gcdTests {

        @ParameterizedTest(name = "returns {2} for {0} and {1}")
        @CsvSource(value = {"5, 10, 5", "45, 90, 45", "4, 4598, 2", "6, 9870, 6", "3, 4958, 1"})
        public void returns_correct_common_divisor(int a, int b, int result) {
            assertEquals(result, numberUtils.gcd(a, b));
        }

        @ParameterizedTest(name = "returns {2} for {0} and {1}")
        @CsvSource(value = {"10, 5, 5", "90, 45, 45", "4598, 4, 2", "9870, 6, 6", "4958, 3, 1"})
        public void returns_correct_common_divisor_if_first_param_is_gt_second_one(int a, int b, int result) {
            assertEquals(result, numberUtils.gcd(a, b));
        }

        @ParameterizedTest(name = "returns {2} for {0} and {1}")
        @CsvSource(value = {"165, 2147483647, 1", "903675927, 799372452, 3"})
        public void works_fine_with_great_numbers(int a, int b, int result) {
            assertEquals(result, numberUtils.gcd(a, b));
        }

        @ParameterizedTest(name = "throws IllegalArgumentException for {0} and {1}")
        @CsvSource(value = {"-165, 2147483647", "903675927, -799372452", "-1, -56"})
        public void throws_exception_with_negative_numbers(int a, int b) {
            assertThrows(IllegalArgumentException.class, () -> numberUtils.gcd(a, b));
        }

        @ParameterizedTest(name = "throws IllegalArgumentException for {0} and {1}")
        @CsvSource(value = {"0, 2147483647", "903675927, 0", "0, 0"})
        public void throws_exception_if_param_equals_zero(int a, int b) {
            assertThrows(IllegalArgumentException.class, () -> numberUtils.gcd(a, b));
        }
    }
}
