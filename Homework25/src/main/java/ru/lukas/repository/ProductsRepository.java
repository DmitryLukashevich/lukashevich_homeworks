package ru.lukas.repository;

import ru.lukas.model.Product;

import java.util.List;

/**
 * @author Dmitry Lukashevich
 */
public interface ProductsRepository {

    List<Product> findAll();

    List<Product> findAllByPrice(double price);

    List<Product> findAllByOrdersCount(int ordersCount);
}
