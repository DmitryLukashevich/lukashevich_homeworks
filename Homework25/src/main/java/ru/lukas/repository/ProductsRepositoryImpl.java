package ru.lukas.repository;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import ru.lukas.model.Product;

import java.math.BigDecimal;

import java.util.List;

/**
 * @author Dmitry Lukashevich
 */
public class ProductsRepositoryImpl implements ProductsRepository {

    private final JdbcTemplate jdbcTemplate;

    private static final RowMapper<Product> productRowMapper = (row, rn) -> Product.builder()
            .id(row.getInt("id"))
            .description(row.getString("description"))
            .price(row.getBigDecimal("price"))
            .amount(row.getInt("amount"))
            .build();

    public ProductsRepositoryImpl(DriverManagerDataSource driverManagerDataSource) {
        this.jdbcTemplate = new JdbcTemplate(driverManagerDataSource);
    }

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query("SELECT * FROM product", productRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query("SELECT * FROM product WHERE price = ?::MONEY",
                productRowMapper,
                BigDecimal.valueOf(price));
    }

    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        String SQL_FIND_ALL_BY_ORDERS_COUNT =
                "SELECT p.id, p.description, p.price, p.amount FROM product p " +
                        "INNER JOIN \"order\" o ON o.product_id = p.id " +
                        "GROUP BY (p.id, p.description, p.price, p.amount) " +
                        "HAVING count(o.product_id) = ?";

        return jdbcTemplate.query(SQL_FIND_ALL_BY_ORDERS_COUNT, productRowMapper, ordersCount);
    }
}
