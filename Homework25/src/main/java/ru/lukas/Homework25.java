package ru.lukas;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import ru.lukas.repository.ProductsRepository;
import ru.lukas.repository.ProductsRepositoryImpl;

/**
 * @author Dmitry Lukashevich
 */
public class Homework25 {

    private static final String url = "jdbc:postgresql://localhost:5432/postgres";

    private static final DriverManagerDataSource driverManager =
            new DriverManagerDataSource(url, "postgres", "root");

    private static final ProductsRepository productsRepository =
            new ProductsRepositoryImpl(driverManager);

    public static void main(String[] args) {
        System.out.println("==========> All products <==========");
        productsRepository.findAll().forEach(System.out::println);
        System.out.println();

        System.out.println("==========> All products with price $46.00 <==========");
        productsRepository.findAllByPrice(46.00).forEach(System.out::println);
        System.out.println();

        System.out.println("==========> All products that have been ordered two times <==========");
        productsRepository.findAllByOrdersCount(2).forEach(System.out::println);
    }
}
