package ru.lukas.model;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author Dmitry Lukashevich
 */
@Data
@Builder
public class Product {

    private int id;
    private String description;
    private BigDecimal price;
    private int amount;
}
