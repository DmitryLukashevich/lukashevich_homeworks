package ru.lukas;

import ru.lukas.model.User;
import ru.lukas.repository.FileUserRepositoryImpl;
import ru.lukas.repository.UserRepository;

public class Attestation01 {

    public static void main(String[] args) {
        UserRepository userRepository = new FileUserRepositoryImpl("users.db");

        System.out.println("==========> Creating and saving user with ID 90000");
        User user = new User(90000, "Helga", 56, true);
        userRepository.save(user);
        userRepository.findById(90000).ifPresent(System.out::println);
        System.out.println("====================================>");

        System.out.println("==========> Updating the user with ID 90000");
        user.setName("George");
        user.setAge(19);
        user.setWorker(false);
        userRepository.update(user);
        userRepository.findById(90000).ifPresent(System.out::println);
        System.out.println("====================================>");

        System.out.println("==========> Deleting the user with ID 90000");
        userRepository.deleteById(90000);
        userRepository.findById(90000).ifPresent(System.out::println);
        System.out.println("====================================>");

        System.out.println("==========> Testing different search methods");
        System.out.println(userRepository.findByAge(19));
        System.out.println(userRepository.findByIsWorker(true));
        userRepository.findById(59).ifPresent(System.out::println);
        System.out.println("====================================>");
    }
}
