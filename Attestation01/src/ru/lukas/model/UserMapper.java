package ru.lukas.model;

/**
 * @author Dmitry Lukashevich
 */
public class UserMapper {

    private static final int ID = 0;
    private static final int NAME = 1;
    private static final int AGE = 2;
    private static final int IS_WORKER = 3;

    public User toUser(String data) {
        User user = new User();
        String[] columns = data.split("\\|");

        user.setId(Integer.parseInt(columns[ID]));
        user.setName(columns[NAME]);
        user.setAge(Integer.parseInt(columns[AGE]));
        user.setWorker(Boolean.parseBoolean(columns[IS_WORKER]));

        return user;
    }

    public String toData(User user) {
        String[] data = new String[4];

        data[ID] = String.valueOf(user.getId());
        data[NAME] = user.getName();
        data[AGE] = String.valueOf(user.getAge());
        data[IS_WORKER] = String.valueOf(user.isWorker());

        return String.join("|", data);
    }
}
