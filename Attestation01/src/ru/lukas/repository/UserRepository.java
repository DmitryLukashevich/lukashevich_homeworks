package ru.lukas.repository;

import ru.lukas.model.User;

import java.util.List;
import java.util.Optional;

/**
 * @author Dmitry Lukashevich
 */
public interface UserRepository {

    List<User> findAll();

    List<User> findByAge(int age);

    List<User> findByIsWorker(boolean isWorker);

    Optional<User> findById(int id);

    void update(User user);

    void deleteById(int id);

    void save(User user);
}
