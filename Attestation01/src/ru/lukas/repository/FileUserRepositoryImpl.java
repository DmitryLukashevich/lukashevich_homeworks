package ru.lukas.repository;

import ru.lukas.model.User;
import ru.lukas.model.UserMapper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Dmitry Lukashevich
 */
public class FileUserRepositoryImpl implements UserRepository {

    private final String filename;
    private final UserMapper userMapper;
    private final Map<Integer, Integer> userIdIndexTable;

    public FileUserRepositoryImpl(String filename) {
        this.filename = filename;
        userMapper = new UserMapper();
        userIdIndexTable = new HashMap<>();
        refreshUserIdIndexTable();
    }

    @Override
    public List<User> findAll() {
        return getUsers(user -> true);
    }

    @Override
    public List<User> findByAge(int age) {
        return getUsers(user -> user.getAge() == age);
    }

    @Override
    public List<User> findByIsWorker(boolean isWorker) {
        return getUsers(user -> user.isWorker() == isWorker);
    }

    /**
     * Retrieves a User with specified ID from text file.
     *
     * This method uses ID indexing mechanism which is considerably faster
     * than simply doing map(userMapper::toUser).filter(user -> user.getId() == id);
     * but we have a space overhead in terms of the HashMap.
     *
     * If searched ID has not been found in user ID index table returns Optional.empty()
     * without reading the file.
     *
     * @param id user ID.
     * @return Optional with User as a value, or empty Optional if no user has been found.
     *
     * @throws IllegalArgumentException if ID is negative number, or equals to zero.
     */
    @Override
    public Optional<User> findById(int id) {
        if (id <= 0) { throw new IllegalArgumentException("ID cannot equal to zero or be a negative number"); }
        if (!userIdIndexTable.containsKey(id)) { return Optional.empty(); }

        List<User> user = consumeReaderStream(lines -> lines.skip(userIdIndexTable.get(id))
                .findFirst().map(userMapper::toUser).stream()
                .collect(Collectors.toList()));

        return Optional.of(user.get(0));
    }

    /**
     * Updates user's data and rewrites text file. Refreshes user ID index table
     *
     * If the user has not been found in the file, appends that user to the end of the file
     * without rewriting it.
     *
     * If the user's data is identical to one in the text file, does nothing
     *
     * @param user user to update.
     *
     * @throws IllegalArgumentException if user ID is negative number, or equals to zero.     *
     */
    @Override
    public void update(User user) {
        Optional<User> userFromFile = findById(user.getId());

        if (userFromFile.isPresent()) {
            if (userFromFile.get().equals(user)) { return; }

            List<User> users = findAll().stream()
                    .map(oldUser -> oldUser.getId() == user.getId() ? user : oldUser)
                    .collect(Collectors.toList());

            saveAll(users, false);
        } else {
            save(user);
        }
    }

    /**
     * Deletes the user with specified ID from text file.
     * Rewrites the file and refreshes user ID index table after that.
     *
     * Does nothing if no ID has been found in index table,
     *
     * @param id user ID.
     *
     * @throws IllegalArgumentException if ID is negative number, or equals to zero.
     */
    @Override
    public void deleteById(int id) {
        if (userIdIndexTable.containsKey(id)) {
            List<User> users = findAll().stream().filter(user -> user.getId() != id)
                    .collect(Collectors.toList());

            saveAll(users, false);
        }
    }

    @Override
    public void save(User user) {
        if (userIdIndexTable.containsKey(user.getId())) { return; } // prevent duplicates

        userIdIndexTable.put(user.getId(), userIdIndexTable.size()); // indexing new user ID
        saveAll(List.of(user), true);
    }

    private void saveAll(List<User> users, boolean isAppended) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename, isAppended))) {
            String data = users.stream().map(userMapper::toData)
                    .collect(Collectors.joining(System.lineSeparator()));

            bw.write(data); // writing all lines at once
            bw.newLine();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        } finally {
            if (!isAppended) { refreshUserIdIndexTable(); } // refresh only when rewriting the file
        }
    }

    private List<User> getUsers(Predicate<User> predicate) {
        return consumeReaderStream(lines -> lines.map(userMapper::toUser)
                .filter(predicate)
                .collect(Collectors.toList()));
    }

    private List<User> consumeReaderStream(Function<Stream<String>, List<User>> function) {
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
           return function.apply(br.lines());
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private void refreshUserIdIndexTable() {
        userIdIndexTable.clear();
        findAll().forEach(user -> userIdIndexTable.put(user.getId(), userIdIndexTable.size()));
    }
}
