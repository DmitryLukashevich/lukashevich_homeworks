package ru.lukas;

import java.util.Scanner;

public class Homework05 {

    private static final Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Enter a number sequence that contains at least one number and ends with -1:");
        System.out.println("The smallest digit of all numbers in the sequence is: " + findSmallestDigit());
    }

    private static int findSmallestDigit() {
        int number = sc.nextInt();
        int smallestDigit = 10;

        if (number == -1) {
            throw new RuntimeException("the number sequence should contain at least one number");
        }

        while (number != -1) {
            while (number > 0) {
                int lastDigit = number % 10;
                number /= 10;

                if (lastDigit < smallestDigit) {
                    smallestDigit = lastDigit;
                }
            }

            number = sc.nextInt();
        }

        return smallestDigit;
    }
}
