package ru.lukas.repository;

import ru.lukas.model.User;

import java.util.List;

/**
 * DAO interface.
 *
 * @author Dmitry Lukashevich
 */
public interface UserRepository {

    List<User> findAll();

    void save(User user);

    List<User> findByAge(int age);

    List<User> findByIsWorkerIsTrue();
}
