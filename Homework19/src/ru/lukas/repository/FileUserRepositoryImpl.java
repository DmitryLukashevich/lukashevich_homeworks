package ru.lukas.repository;

import ru.lukas.model.User;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * An implementation of the UserRepository DAO interface. Plain text file serves as database here.
 *
 * @author Dmitry Lukashevich
 */
public class FileUserRepositoryImpl implements UserRepository {

    private final String filename;

    public FileUserRepositoryImpl(String filename) {
        this.filename = filename;
    }

    @Override
    public List<User> findAll() {
        return getUsers(x -> true);
    }

    @Override
    public void save(User user) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename, true))) {
            bw.write(user.getName() + "|" + user.getAge() +"|" + user.isWorker());
            bw.newLine();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public List<User> findByAge(int age) {
        return getUsers(user -> user.getAge() == age);
    }

    @Override
    public List<User> findByIsWorkerIsTrue() {
        return getUsers(User::isWorker);
    }

    private List<User> getUsers(Predicate<User> predicate) {
        List<User> users = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line = br.readLine();

            while (line != null) {
                String[] userData = line.split("\\|");
                User user = new User(userData[0],           // name
                        Integer.parseInt(userData[1]),      // age
                        Boolean.parseBoolean(userData[2])); // isWorker

                if (predicate.test(user)) { users.add(user); }
                line = br.readLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }

        return users;
    }
}
