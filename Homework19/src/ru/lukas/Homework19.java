package ru.lukas;

import ru.lukas.model.User;
import ru.lukas.repository.UserRepository;
import ru.lukas.repository.FileUserRepositoryImpl;

/**
 * @author Dmitry Lukashevich
 */
public class Homework19 {

    public static void main(String[] args) {
        UserRepository userRepository = new FileUserRepositoryImpl("users.db");

        System.out.println("===============> All users <===============");
        for (User user : userRepository.findAll()) {
            System.out.println("=====> " + user);
        }

        System.out.println("===============> Users with age 24 <===============");
        for (User user: userRepository.findByAge(24)) {
            System.out.println("=====> " + user);
        }

        System.out.println("===============> Workers <===============");
        for (User user: userRepository.findByIsWorkerIsTrue()) {
            System.out.println("=====> " + user);
        }

        userRepository.save(new User("Petr", 19, false));
    }
}
