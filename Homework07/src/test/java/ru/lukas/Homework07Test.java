package ru.lukas;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import org.junit.Test;

public class Homework07Test {

    @Test
    public void shouldReturnLeastFrequentNumberInSequence() {
        int[] sequence = {
                -100, -100, -56, 28, 34, 100, 100, 100, 98, -2, -2, 28,
                -56, -56, -100, 28, 34, 0, 98, 11, 0, 28, 28, -1, 48, 12
        };

        assertThat(Homework07.findLeastFrequentNumber(sequence), equalTo(11));
    }
}
