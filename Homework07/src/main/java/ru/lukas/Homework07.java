package ru.lukas;

import java.util.Arrays;

/**
 * @author Dmitry Lukashevich
 */
public class Homework07 {

    public static void main(String[] args) {
        int[] sequence = { -100, 94, 0, -58, 0, -100, 100, 94, 10, 100, 10, -1 };

        System.out.print("The least frequent number in a sequence " + Arrays.toString(sequence) + " is ");
        System.out.println(findLeastFrequentNumber(sequence));
    }

    /*
     * Solves the problem with linear O(N) time complexity.
     *
     * We could also solve this with nested loops but that would result in quadratic time complexity O(N^2)
     */
    public static int findLeastFrequentNumber(int[] sequence) {
        /*
         * Initializing hash map where the key(index) represents the number
         * and the value(element with that index) represents the frequency of the number
         */
        int[] hashMap = new int[201];
        // Necessary variables to find leastFrequentNumber
        int frequency = Integer.MAX_VALUE;
        int leastFrequentNumber = -1;

        for (int i = 0; i < sequence.length && sequence[i] != -1; i++) {
            /*
             * Since index in array cannot be negative, for the range -100...100 we use an array with size 201.
             * Index = 100 represents number 0, to get the correct index for our number we do -> number + 100.
             * For example: to get the index of negative number -100 we add 100 to it and get 0 as a result,
             * for the number -40 the index is 60 (-40 + 100 = 60)
             * for the number 25 the index is 125 and so on...
             */
            hashMap[sequence[i] + 100]++;
        }

        /*
         * or cryptic oneliner :)))))
         *     for (int i = 0; i < sequence.length && sequence[i] != -1; hashMap[sequence[i++] + 100]++);
         */

        /*
         * Now we're basically facing the "find-the-smallest-number-in-array" problem.
         * We find the smallest value—it represents the frequency of the number—and then we
         * need to get the number itself which is index - 100.
         */
        for (int i = 0; i < hashMap.length; i++) {
            if (hashMap[i] < frequency && hashMap[i] != 0) {
                frequency = hashMap[i];
                leastFrequentNumber = i - 100;
            }
        }

        return leastFrequentNumber;
    }
}
