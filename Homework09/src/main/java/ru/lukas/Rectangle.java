package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class Rectangle extends Figure {
    /*
     * Since there's no setters in this class and our fields cannot be changed in any way
     * we may as well make 'em final
     */
    private final int sideA;
    private final int sideB;

    public Rectangle(int x, int y, int sideA, int sideB) {
        super(x, y);
        this.sideA = sideA;
        this.sideB = sideB;
    }

    @Override
    public int getPerimeter() {
        return (sideA + sideB) * 2;
    }
}

