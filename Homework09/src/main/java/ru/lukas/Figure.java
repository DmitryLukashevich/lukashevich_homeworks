package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class Figure {
    /*
     * Since there's no setters in this class and our fields cannot be changed in any way
     * we may as well make 'em final
     */
    private final int x;
    private final int y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getPerimeter() {
        return 0;
    }
}
