package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class Ellipse extends Figure {
    /*
     * Since there's no setters in this class and our fields cannot be changed in any way
     * we may as well make 'em final
     */
    private final int radiusA;
    private final int radiusB;

    public Ellipse(int x, int y, int radiusA, int radiusB) {
        super(x, y);
        this.radiusA = radiusA;
        this.radiusB = radiusB;
    }

    @Override
    public int getPerimeter() {
        /*
         * Since Math.sqrt returns floating-point number we cannot divide it by 2 without loosing accuracy.
         * To preserve it We can either divide it by 2.0, 2.0f or shift one bit right >> 1.
         */
        return (int) Math.round(2 * Math.PI * Math.sqrt((Math.pow(radiusA, 2) + Math.pow(radiusB, 2)) / 2.0));
    }
}
