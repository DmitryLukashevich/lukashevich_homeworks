package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class Homework09 {

    public static void main(String[] args) {
        Figure rectangle = new Rectangle(10, 10, 12, 8);
        Figure square = new Square(40, 10, 14);
        Figure ellipse = new Ellipse(10, 40, 23, 14);
        Figure circle = new Circle(120, 200, 42);

        System.out.println("Perimeter of rectangle with sides 12 and 8 is " + rectangle.getPerimeter());
        System.out.println("Perimeter of square with sides 14 is " + square.getPerimeter());
        System.out.println("Perimeter(circumference) of ellipse with radiuses 23 and 14 is " + ellipse.getPerimeter());
        System.out.println("Perimeter(circumference) of circle with radius 42 is " + circle.getPerimeter());
    }
}
