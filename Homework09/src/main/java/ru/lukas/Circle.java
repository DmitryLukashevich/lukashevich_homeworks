package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class Circle extends Ellipse {

    public Circle(int x, int y, int radiusA) {
        super(x, y, radiusA, radiusA);
    }
}
