package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class Square extends Rectangle {

    public Square(int x, int y, int sideA) {
        super(x, y, sideA, sideA);
    }
}
