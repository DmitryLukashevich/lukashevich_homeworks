package ru.lukas;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class Homework09Test {

    @Test
    public void figureShouldPrintItsPerimeterAsZero() {
        Figure figure = new Figure(12, 10);

        assertThat(figure.getPerimeter(), equalTo(0));
    }

    @Test
    public void ellipseShouldPrintItsPerimeter() {
        Figure ellipse1 = new Ellipse(10, 10, 45, 12);
        Figure ellipse2 = new Ellipse(5, 7, 34, 72);
        Figure ellipse3 = new Ellipse(1, 0, 10, 27);

        assertThat(ellipse1.getPerimeter(), equalTo(207));
        assertThat(ellipse2.getPerimeter(), equalTo(354));
        assertThat(ellipse3.getPerimeter(), equalTo(128));
    }

    @Test
    public void circleShouldPrintItsPerimeter() {
        Figure circle1 = new Circle(20, 10, 50);
        Figure circle2 = new Circle(12, 0, 127);
        Figure circle3 = new Circle(1, 1, 12);

        assertThat(circle1.getPerimeter(), equalTo(314));
        assertThat(circle2.getPerimeter(), equalTo(798));
        assertThat(circle3.getPerimeter(), equalTo(75));
    }

    @Test
    public void rectangleShouldPrintItsPerimeter() {
        Figure rectangle1 = new Rectangle(1, 2, 27, 34);
        Figure rectangle2 = new Rectangle(0, 0, 15, 100);
        Figure rectangle3 = new Rectangle(1, 1, 67, 18);

        assertThat(rectangle1.getPerimeter(), equalTo(122));
        assertThat(rectangle2.getPerimeter(), equalTo(230));
        assertThat(rectangle3.getPerimeter(), equalTo(170));
    }

    @Test
    public void squareShouldPrintItsPerimeter() {
        Figure square1 = new Square(7, 5, 70);
        Figure square2 = new Square(2, 4, 64);
        Figure square3 = new Square(12, 0, 20);

        assertThat(square1.getPerimeter(), equalTo(280));
        assertThat(square2.getPerimeter(), equalTo(256));
        assertThat(square3.getPerimeter(), equalTo(80));
    }
}
