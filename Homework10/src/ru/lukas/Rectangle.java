package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class Rectangle extends Figure {

    private final int sideA;
    private final int sideB;

    public Rectangle(int x, int y, int sideA, int sideB) {
        super(x, y);
        this.sideA = sideA;
        this.sideB = sideB;
    }

    @Override
    public int getPerimeter() {
        return (sideA + sideB) * 2;
    }
}

