package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public interface Movable {

    void moveTo(int x, int y);
}
