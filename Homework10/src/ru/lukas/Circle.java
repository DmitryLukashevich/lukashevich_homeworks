package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class Circle extends Ellipse implements MovableFigure {

    public Circle(int x, int y, int radius) {
        super(x, y, radius, radius);
    }

    @Override
    public void moveTo(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /*
     * Not needed in Homework 10. Used only to illustrate that moveTo method works properly
     */
    @Override
    public String toString() {
        return "Circle{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
