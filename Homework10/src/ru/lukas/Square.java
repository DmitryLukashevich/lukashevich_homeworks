package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class Square extends Rectangle implements MovableFigure {

    public Square(int x, int y, int side) {
        super(x, y, side, side);
    }

    @Override
    public void moveTo(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /*
     * Not needed in Homework 10. Used only to illustrate that moveTo method works properly
     */
    @Override
    public String toString() {
        return "Square{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
