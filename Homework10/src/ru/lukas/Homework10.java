package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class Homework10 {

    public static void main(String[] args) {
        MovableFigure[] figures = {
                new Circle(1, 1, 15),
                new Circle(10,20, 10),
                new Circle(50, 8, 20),
                new Square(100, 25, 10),
                new Square(30, 54, 25),
                new Square(100, 0, 5)
        };

        System.out.println("=================== Elements before moving ===================");
        printMovableElements(figures);

        for (MovableFigure figure : figures) {
            figure.moveTo(200, 200);
        }

        System.out.println("=============== The same elements after moving ===============");
        printMovableElements(figures);
    }

    /*
     * Not required in Homework 10. Used to illustrate that all Movable objects in the array
     * have moved to desired coordinates.
     */
    private static void printMovableElements(MovableFigure[] elements) {
        for (int i = 0; i < elements.length; i++) {
            System.out.println("Element " + i + " is: " + elements[i]);
        }
        System.out.println();
    }
}
