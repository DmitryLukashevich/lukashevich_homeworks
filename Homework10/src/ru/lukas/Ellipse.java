package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public class Ellipse extends Figure {

    private final int radiusA;
    private final int radiusB;

    public Ellipse(int x, int y, int radiusA, int radiusB) {
        super(x, y);
        this.radiusA = radiusA;
        this.radiusB = radiusB;
    }

    @Override
    public int getPerimeter() {
        return (int) Math.round(2 * Math.PI * Math.sqrt((Math.pow(radiusA, 2) + Math.pow(radiusB, 2)) / 2.0));
    }
}
