package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public interface MovableFigure extends Movable {

    int getPerimeter();
}
