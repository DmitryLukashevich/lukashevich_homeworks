package ru.lukas;

/**
 * @author Dmitry Lukashevich
 */
public interface ByCondition {

    boolean isOk(int number);
}
