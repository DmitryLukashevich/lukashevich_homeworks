package ru.lukas;

import java.util.Arrays;

/**
 * @author Dmitry Lukashevich
 */
public class Homework13 {

    public static void main(String[] args) {
        int[] array = { 125, -18, 0, 162, 165, 90, 91, 58, 23, 57, -1, 1 };

        // Filters an initial array and returns a new array containing even numbers
        int[] onlyEvenNumbersArray = Sequence.filter(array, e -> e % 2 == 0);

        // Filters an initial array and returns a new array that contains numbers with even sum of digits
        int[] sumOfDigitsIsEvenArray = Sequence.filter(array, e -> {
            int sum = 0;

            while (e != 0) {
                sum += e % 10;
                e /= 10;
            }

            return sum % 2 == 0;
        });

        // =============================== Testing ==================================================
        System.out.println("=====> An initial array: " + Arrays.toString(array));
        System.out.println("=====> The same array that contains even numbers only: " +
                Arrays.toString(onlyEvenNumbersArray));
        System.out.println("=====> The same array containing numbers with an even sum of digits: " +
                Arrays.toString(sumOfDigitsIsEvenArray));
    }
}
