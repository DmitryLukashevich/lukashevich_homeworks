package ru.lukas;

import java.util.Arrays;

/**
 * @author Dmitry Lukashevich
 */
public class Sequence {

    /**
     * An util static method to filter through an array using ByCondition interface as a predicate.
     * Doesn't change the original array, returns a copy of it with filtered elements.
     *
     * O(N) time complexity.
     *
     * @param array - an array to filter through
     * @param condition - a predicate
     * @return a new copy of array that contains only elements that satisfy the predicate
     */
    public static int[] filter(int[] array, ByCondition condition) {
        int[] tmp = new int[array.length];
        int nextElement = 0;

        for (int element : array) {
            if (condition.isOk(element)) {
                tmp[nextElement++] = element;
            }
        }
        // Truncating zeroes, if any
        return Arrays.copyOf(tmp, nextElement);
    }
}
