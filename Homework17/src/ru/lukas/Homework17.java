package ru.lukas;

import java.util.HashMap;
import java.util.Map;

public class Homework17 {

    public static void main(String[] args) {
        String text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

        printWordCount(text);
    }

    private static void printWordCount(String text) {
        Map<String, Integer> wordMap = new HashMap<>();

        for (String word : text.toLowerCase().split(" ")) {
            wordMap.put(word, wordMap.getOrDefault(word, 0) + 1);
        }

        System.out.println("=====> " + text);
        System.out.println("=====> Word count: <=====");

        for (Map.Entry<String, Integer> entry : wordMap.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }
    }
}
